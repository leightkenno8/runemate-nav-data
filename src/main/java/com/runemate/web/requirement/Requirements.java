package com.runemate.web.requirement;

import com.runemate.web.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;
import org.jetbrains.annotations.*;

@UtilityClass
public class Requirements {

    public static boolean SKIP = false;

    public static @NotNull Requirement none() {
        return new NoneRequirement();
    }

    public static @NotNull Requirement and(@NonNull Requirement... requirements) {
        return new AndRequirement(Arrays.stream(requirements));
    }

    public static @NotNull Requirement and(@NonNull Stream<Requirement> requirements) {
        return new AndRequirement(requirements);
    }

    public static @NotNull Requirement or(@NonNull Requirement... requirements) {
        return new OrRequirement(Arrays.stream(requirements));
    }

    public static @NotNull Requirement or(@NonNull Stream<Requirement> requirements) {
        return new OrRequirement(requirements);
    }

    public static @NotNull Requirement not(@NonNull Requirement requirement) {
        return requirement.not();
    }

    private static final class NoneRequirement implements Requirement {

        @Override
        public boolean satisfy(@NonNull WebContext context) {
            return true;
        }

        @Override
        public String toString() {
            return "None";
        }

        @Override
        public int type() {
            return Type.NONE;
        }
    }
}
