package com.runemate.web.requirement;

import com.runemate.web.*;
import java.util.regex.*;
import lombok.*;

@Value
public class ItemRequirement implements Requirement {

    @NonNull Pattern itemName;
    int quantity;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasInventoryItem(itemName, quantity);
    }

    @Override
    public int type() {
        return Type.ITEM;
    }
}
