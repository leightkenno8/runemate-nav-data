package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class TeleportsEnabled implements Requirement {

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.isAllowingTeleports();
    }

    @Override
    public int type() {
        return Type.TELEPORTS_ENABLED;
    }
}
