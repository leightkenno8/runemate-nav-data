package com.runemate.web.requirement;

import com.runemate.web.*;
import com.runemate.web.util.*;
import lombok.*;
import lombok.experimental.*;

@Value
public class VarbitRequirement implements Requirement {

    int varbitIndex;
    int varbitValue;
    IntOp operation;

    @Tolerate
    public VarbitRequirement(final int varbitIndex, final int varbitValue) {
        this(varbitIndex, varbitValue, IntOp.EQ);
    }

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasVarbit(varbitIndex, varbitValue, operation);
    }

    @Override
    public int type() {
        return Type.VARBIT;
    }
}
