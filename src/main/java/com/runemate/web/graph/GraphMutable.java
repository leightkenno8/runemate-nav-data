package com.runemate.web.graph;

import com.runemate.web.edge.*;
import java.util.stream.*;
import lombok.*;

public interface GraphMutable<V> {

    void addVertices(@NonNull Stream<V> vertices);

    void addVertex(@NonNull V vertex);

    void addEdges(@NonNull Stream<Edge<V>> edges);

    void addEdge(@NonNull Edge<V> edge);

    void addNonConflictingEdges(@NonNull Stream<Edge<V>> edges);
}
