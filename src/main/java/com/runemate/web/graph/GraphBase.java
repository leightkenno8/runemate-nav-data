package com.runemate.web.graph;

import com.runemate.web.util.*;
import java.util.*;


public abstract class GraphBase<V> implements Graph<V> {

    @Override
    public /*final*/ boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Graph)) {
            return false;
        }
        Graph<?> that = (Graph<?>) o;
        return vertexCount() == that.vertexCount() && edgeCount() == that.edgeCount() &&
            Streams.equals(vertices(), that.vertices()) && Streams.equals(edges(), that.edges());
    }

    @Override
    public /*final*/ int hashCode() {
        return Objects.hash(Streams.hashCode(vertices()), Streams.hashCode(edges()));
    }

    @Override
    public final String toString() {
        return new StringJoiner(", ", Graph.class.getSimpleName() + "[", "]")
            .add("vertexCount=" + vertexCount())
            .add("edgeCount=" + edgeCount())
            .toString();
    }
}
