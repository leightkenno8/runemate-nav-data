package com.runemate.web.graph;

import com.runemate.web.edge.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;
import org.jetbrains.annotations.*;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public final class MutableGraph<V> extends GraphBase<V> implements GraphMutable<V> {

    Set<V> vertices;
    Map<V, Collection<Edge<V>>> edges;

    @NonFinal
    volatile int hashCode;

    public MutableGraph() {
        this.vertices = new LinkedHashSet<>();
        this.edges = new HashMap<>();
    }

    public MutableGraph(int initialVertexCapacity) {
        this.vertices = new LinkedHashSet<>(initialVertexCapacity);
        this.edges = new HashMap<>(initialVertexCapacity);
    }

    public MutableGraph(@NonNull Collection<V> vertices, @NonNull Collection<Edge<V>> edges) {
        this.vertices = new LinkedHashSet<>(vertices.size(), 1f);
        this.vertices.addAll(vertices);
        this.edges = new HashMap<>(vertices.size(), 1f);
        addEdges(edges.stream());
    }

    public synchronized int compact() {
        List<V> verticesToRemove = vertices.stream().filter(v -> !edges.containsKey(v)).collect(Collectors.toList());
        for (V vertex : verticesToRemove) {
            vertices.remove(vertex);
            edges.remove(vertex);
        }
        int removedEdges = 0;
        for (Collection<Edge<V>> edgeCollection : edges.values()) {
            List<Edge<V>> toRemove = edgeCollection.stream()
                .filter(edge -> !vertices.contains(edge.destination()))
                .collect(Collectors.toList());
            removedEdges += toRemove.size();
            edgeCollection.removeAll(toRemove);
            if (edgeCollection instanceof ArrayList<?>) {
                ((ArrayList<Edge<V>>) edgeCollection).trimToSize();
            }
        }
        edges.values().forEach(e1 -> e1.removeIf(e -> !vertices.contains(e.destination())));
        hashCode = 0;
        return removedEdges;
    }

    @Override
    public long vertexCount() {
        return vertices.size();
    }

    @Override
    public boolean containsVertex(@NonNull V vertex) {
        return vertices.contains(vertex);
    }

    @Override
    public @NotNull Stream<V> vertices() {
        return vertices.stream();
    }

    @Override
    public void addVertices(@NonNull Stream<V> vertices) {
        vertices.forEachOrdered(this::_addVertex);
        hashCode = 0;
    }

    @Override
    public void addVertex(@NonNull V vertex) {
        vertices.add(vertex);
        hashCode = 0;
    }

    private void _addVertex(@NonNull V vertex) {
        vertices.add(vertex);
    }

    @Override
    public @NotNull Stream<Edge<V>> edges() {
        return edges.values()
            .stream().flatMap(Collection::stream);
    }

    @Override
    public @NotNull Stream<Edge<V>> edges(@NonNull V vertex) {
        Collection<Edge<V>> value = edges.get(vertex);
        if (value == null) {
            return Stream.empty();
        }
        return value.stream();
    }

    @Override
    public void addEdges(@NonNull Stream<Edge<V>> edges) {
        edges.forEachOrdered(this::_addEdge);
        hashCode = 0;
    }

    @Override
    public void addEdge(@NonNull Edge<V> edge) {
        _addEdge(edge);
        hashCode = 0;
    }

    private void _addEdge(@NonNull Edge<V> edge) {
        V origin = edge.origin();
        Collection<Edge<V>> edges = this.edges.computeIfAbsent(origin, k -> new LinkedHashSet<>(4));
        Optional<Edge<V>> conflict;
        if (edges.contains(edge)) {
            throw new IllegalStateException(edge + " already exists for " + origin);
        } else if ((conflict = edges.stream().filter(e -> e.destination().equals(edge.destination())).findAny()).isPresent()) {
            throw new IllegalStateException("Conflicting edge exists for " + origin + ", existing=" + conflict.get() + ", new=" + edge);
        }
        edges.add(edge);
    }

    public void removeEdge(@NonNull Edge<V> edge) {
        V origin = edge.origin();
        if (edges.containsKey(origin)) {
            edges.get(origin).remove(edge);
        }
    }

    public void removeVertex(@NonNull V vertex) {
        vertices.remove(vertex);
        if (edges.containsKey(vertex)) {
            var remove = edges(vertex).map(Edge::destination).flatMap(this::edges)
                .filter(edge -> vertex.equals(edge.destination()))
                .collect(Collectors.toSet());

            remove.forEach(this::removeEdge);
        }

        edges.remove(vertex);
    }

    @Override
    public void addNonConflictingEdges(@NonNull Stream<Edge<V>> edges) {
        edges = edges.filter(edge -> {
            final var _edges = this.edges.computeIfAbsent(edge.origin(), k -> new LinkedHashSet<>(4));
            if (_edges.contains(edge)) {
                return false;
            }
            return _edges.stream().noneMatch(e -> e.destination().equals(edge.destination()));
        });
        addEdges(edges);
    }

    @Override
    public long edgeCount() {
        return edges.values()
            .stream().mapToLong(Collection::size).sum();
    }

    @Override
    public boolean containsEdge(@NonNull Edge<V> edge) {
        return edges.values()
            .stream().anyMatch(value -> value.contains(edge));
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = super.hashCode();
        }
        return hashCode;
    }

    @Override
    public void close() throws Exception {
        // no-op
    }
}
