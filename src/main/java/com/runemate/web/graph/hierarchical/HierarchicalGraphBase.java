package com.runemate.web.graph.hierarchical;

import com.runemate.web.graph.*;
import com.runemate.web.util.*;
import java.util.*;

public abstract class HierarchicalGraphBase<G extends Graph<V>, V> implements HierarchicalGraph<G, V> {

    @Override
    public final boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HierarchicalGraph)) {
            return false;
        }
        HierarchicalGraph<?, ?> that = (HierarchicalGraph<?, ?>) o;
        return Streams.equals(vertices(), that.vertices()) && Streams.equals(edges(), that.edges());
    }

    @Override
    public /*final*/ int hashCode() {
        return Objects.hash(Streams.hashCode(vertices()), Streams.hashCode(edges()));
    }

    @Override
    public /*final*/ String toString() {
        return new StringJoiner(", ", HierarchicalGraph.class.getSimpleName() + "[", "]")
            .add("vertexCount=" + vertexCount())
            .add("edgeCount=" + edgeCount())
            .toString();
    }
}
