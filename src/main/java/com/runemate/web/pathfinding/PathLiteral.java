package com.runemate.web.pathfinding;

import com.runemate.web.edge.*;
import java.util.*;

public final class PathLiteral<V> implements Path<V> {

    private final List<V> vertices;
    private final List<Edge<V>> edges;
    private final float cost;

    public PathLiteral(List<V> vertices, List<Edge<V>> edges) {
        this.vertices = vertices;
        this.edges = edges;
        cost = (float) edges.stream().mapToDouble(Edge::cost).sum();
    }

    @Override
    public List<V> vertices() {
        return vertices;
    }

    @Override
    public List<Edge<V>> edges() {
        return edges;
    }

    public double cost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Path{" +
            "origin=" + vertices.get(0) +
            ", destination=" + vertices.get(vertices.size() - 1) +
            ", vertices=" + vertices.size() +
            ", edges=" + edges.size() +
            ", cost=" + cost +
            '}';
    }
}
