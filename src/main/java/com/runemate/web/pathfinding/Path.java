package com.runemate.web.pathfinding;

import com.runemate.web.edge.*;
import java.util.*;

public interface Path<V> {

    List<V> vertices();

    List<Edge<V>> edges();

    double cost();

}
