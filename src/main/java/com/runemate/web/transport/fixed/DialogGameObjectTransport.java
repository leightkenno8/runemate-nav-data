package com.runemate.web.transport.fixed;

import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString(callSuper = true)
public class DialogGameObjectTransport extends GameObjectTransport {

    @NonNull Pattern dialogPattern;

}
