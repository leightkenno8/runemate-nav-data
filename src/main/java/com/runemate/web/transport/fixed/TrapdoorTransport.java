package com.runemate.web.transport.fixed;

import com.runemate.web.model.*;
import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString(callSuper = true)
public class TrapdoorTransport extends FixedTransport {

    @NonNull Coordinate trapdoorPosition;
    @Builder.Default
    Pattern trapdoorName = Pattern.compile("^Trapdoor$");
    @Builder.Default
    Pattern trapdoorAction = Pattern.compile("^(Open|Climb-down)$");

    @NonNull Coordinate ladderPosition;
    @Builder.Default
    Pattern ladderName = Pattern.compile("^Ladder$");
    @Builder.Default
    Pattern ladderAction = Pattern.compile("^Climb-up$");

}
