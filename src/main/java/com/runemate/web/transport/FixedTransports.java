package com.runemate.web.transport;

import com.runemate.web.data.*;
import com.runemate.web.model.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import lombok.experimental.*;

@UtilityClass
public class FixedTransports {

    public Collection<FixedTransport> load() {
        final List<FixedTransport> transports = new ArrayList<>();

        transports.addAll(DiagonalObjects.getAll());
        transports.addAll(AgilityShortcuts.getAll());
        transports.addAll(CharterShips.getAll());
        transports.addAll(DungeonEntrances.getAll());
        transports.addAll(Npcs.getAll());
        transports.addAll(FairyRing.transports());
        transports.addAll(FairyRing.zanarisTransports());
        transports.addAll(SpiritTree.transports());
        transports.addAll(MushTree.transports());
        transports.addAll(Trapdoors.getAll());

        //Dwarf cannon ladders
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2570, 3442, 0))
            .destination(new Coordinate(2570, 3442, 1))
            .objectPosition(new Coordinate(2570, 3441, 0))
            .objectName("Ladder")
            .objectAction("Climb-up")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2570, 3442, 1))
            .destination(new Coordinate(2569, 3443, 2))
            .objectPosition(new Coordinate(2570, 3443, 1))
            .objectName("Ladder")
            .objectAction("Climb-up")
            .build());
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2570, 3442, 1))
            .destination(new Coordinate(2570, 3442, 0))
            .objectPosition(new Coordinate(2570, 3441, 1))
            .objectName("Ladder")
            .objectAction("Climb-down")
            .build());

        //Tower of life door
        transports.add(GameObjectTransport.builder()
            .source(new Coordinate(2649, 3226, 0))
            .objectPosition(new Coordinate(2649, 3225, 0))
            .destination(new Coordinate(2649, 3224, 0))
            .objectName("Tower door")
            .objectAction("Open")
            .bidirectional(true)
            .build());

        return transports;
    }

}
