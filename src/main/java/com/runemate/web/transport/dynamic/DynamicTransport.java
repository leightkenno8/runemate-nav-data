package com.runemate.web.transport.dynamic;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString
public class DynamicTransport {

    @NonNull Coordinate destination;
    @NonNull Requirement requirement;
    @Builder.Default float cost = 25f;

}
