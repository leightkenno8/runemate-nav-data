package com.runemate.web.data;

import com.runemate.web.model.*;
import java.util.*;
import lombok.*;

/**
 * Altar locations, for use in landmark traversal
 */
public class Altars {

    @Getter(lazy = true)
    private static final List<Coordinate> all = List.of(
        new Coordinate(2695, 3462, 0),
        new Coordinate(1743, 3499, 0),
        new Coordinate(3051, 3497, 1),
        new Coordinate(2529, 3287, 0),
        new Coordinate(3421, 3180, 1),
        new Coordinate(1689, 3792, 2),
        new Coordinate(1497, 3563, 0),
        new Coordinate(1812, 5739, 0),
        new Coordinate(1776, 5467, 1),
        new Coordinate(1310, 3618, 0),
        new Coordinate(2803, 6115, 1),
        new Coordinate(2073, 6169, 2),
        new Coordinate(3263, 6090, 2),
        new Coordinate(3122, 3106, 0),
        new Coordinate(2355, 3171, 1),
        new Coordinate(1733, 3573, 0),
        new Coordinate(3209, 3494, 1),
        new Coordinate(1714, 6114, 0),
        new Coordinate(3280, 2773, 0),
        new Coordinate(3415, 3488, 0),
        new Coordinate(2728, 3283, 0),
        new Coordinate(3742, 4319, 0),
        new Coordinate(2341, 9628, 0),
        new Coordinate(2888, 3510, 1),
        new Coordinate(2156, 3863, 0),
        new Coordinate(3246, 6115, 0),
        new Coordinate(2456, 2838, 2),
        new Coordinate(1812, 5707, 0),
        new Coordinate(2617, 3308, 0),
        new Coordinate(3253, 3485, 0),
        new Coordinate(1617, 3673, 2),
        new Coordinate(3742, 4317, 0),
        new Coordinate(2749, 3495, 1),
        new Coordinate(2996, 3177, 0),
        new Coordinate(2938, 5202, 0),
        new Coordinate(2604, 3209, 0),
        new Coordinate(3746, 4318, 0),
        new Coordinate(2931, 6179, 1),
        new Coordinate(3376, 3284, 0),
        new Coordinate(1283, 3677, 0),
        new Coordinate(3232, 9310, 0),
        new Coordinate(3307, 6180, 0),
        new Coordinate(1546, 3807, 0),
        new Coordinate(3243, 3208, 0),
        new Coordinate(2283, 3428, 0),
        new Coordinate(2607, 3209, 0),
        new Coordinate(3178, 3625, 0)
    );

}
