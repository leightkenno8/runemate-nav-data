package com.runemate.web.data;

import static com.runemate.web.requirement.DiaryRequirement.Difficulty.*;
import static com.runemate.web.requirement.DiaryRequirement.Region.*;

import com.runemate.web.*;
import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.dynamic.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;

/**
 * Defines Item Teleports
 */
@UtilityClass
public class Items {

    public static final Pattern TELEPORT = Pattern.compile("Teleport");
    public static final Pattern RUB = Pattern.compile("Rub");

    @Getter(lazy = true)
    private final Collection<BasicItemTransport> basic = loadBasicItemTransports();

    @Getter(lazy = true)
    private final Collection<DialogItemTransport> dialog = loadDialogItemTransports();

    public List<DialogItemTransport> getDialogItems(@NonNull WebContext context) {
        return getDialog().stream().filter(it -> it.getRequirement().satisfy(context)).collect(Collectors.toList());
    }

    public List<BasicItemTransport> getBasicItems(@NonNull WebContext context) {
        return getBasic().stream().filter(it -> it.getRequirement().satisfy(context)).collect(Collectors.toList());
    }

    public List<DynamicTransport> getAll(@NonNull WebContext context) {
        final var result = new ArrayList<DynamicTransport>();
        result.addAll(getBasicItems(context));
        result.addAll(getDialogItems(context));
        return result;
    }

    public List<DynamicTransport> getAll() {
        final var result = new ArrayList<DynamicTransport>();
        result.addAll(getBasic());
        result.addAll(getDialog());
        return result;
    }

    private List<BasicItemTransport> loadBasicItemTransports() {
        final List<BasicItemTransport> results = new ArrayList<>();

        results.addAll(InventoryItem.transports());
        results.addAll(ChargedItem.getEquippedTransports());

        //Drakan's medallion
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Ver Sinhaza"))
            .destination(new Coordinate(3649, 3230, 0))
            .origin(BasicItemTransport.Origin.INVENTORY)
            .requirement(new ItemRequirement(Pattern.compile("Drakan's medallion"), 1))
            .cost(15)
            .build());
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Ver Sinhaza"))
            .destination(new Coordinate(3649, 3230, 0))
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .requirement(new EquipmentRequirement(Pattern.compile("Drakan's medallion")))
            .cost(15)
            .build());
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Slepe"))
            .destination(new Coordinate(3808, 9700, 1))
            .origin(BasicItemTransport.Origin.INVENTORY)
            .requirement(new ItemRequirement(Pattern.compile("Drakan's medallion"), 1).and(new VarbitRequirement(12416, 1)))
            .cost(25)
            .build());
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Slepe"))
            .destination(new Coordinate(3808, 9700, 1))
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .requirement(new EquipmentRequirement(Pattern.compile("Drakan's medallion")).and(new VarbitRequirement(12416, 1)))
            .cost(25)
            .build());
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Darkmeyer"))
            .destination(new Coordinate(3592, 3337, 0))
            .origin(BasicItemTransport.Origin.INVENTORY)
            .requirement(new ItemRequirement(Pattern.compile("Drakan's medallion"), 1).and(new QuestRequirement("SINS_OF_THE_FATHER", QuestRequirement.State.COMPLETE)))
            .cost(50)
            .build());
        results.add(BasicItemTransport.builder()
            .itemName(Pattern.compile("Drakan's medallion"))
            .itemAction(Pattern.compile("Darkmeyer"))
            .destination(new Coordinate(3592, 3337, 0))
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .requirement(new EquipmentRequirement(Pattern.compile("Drakan's medallion")).and(new QuestRequirement("SINS_OF_THE_FATHER", QuestRequirement.State.COMPLETE)))
            .cost(50)
            .build());

        //Xeric's talisman
        final var xericsTalisman = Map.of(
            Pattern.compile("Xeric's Lookout"), new Coordinate(1576, 3530, 0),
            Pattern.compile("Xeric's Glade"), new Coordinate(1752, 3566, 0),
            Pattern.compile("Xeric's Heart"), new Coordinate(1640, 3674, 0),
            Pattern.compile("Xeric's Inferno"), new Coordinate(1504, 3817, 0)
        );
        for (final var entry : xericsTalisman.entrySet()) {
            either(results, Pattern.compile("Xeric's talisman"), entry.getKey(), entry.getValue());
        }

        //Kharedst's Memoirs
        final var kharedstsMemoirs = Map.of(
            Pattern.compile("The Fisher's Flute"), new Coordinate(1803, 3748, 0),
            Pattern.compile("History and Hearsay"), new Coordinate(1475, 3580, 0),
            Pattern.compile("Jewellery of Jubilation"), new Coordinate(1544, 3761, 0),
            Pattern.compile("A Dark Disposition"), new Coordinate(1681, 3746, 0)
        );
        final var khardestsMemoirsName = Pattern.compile("(Kharedst's memoirs|Book of the dead)");
        for (final var entry : kharedstsMemoirs.entrySet()) {
            either(results, khardestsMemoirsName, entry.getKey(), entry.getValue());
        }

        either(results, Pattern.compile("Mythical cape"), TELEPORT, new Coordinate(2457, 2850, 0));
        either(results, Pattern.compile("Quest point cape"), TELEPORT, new Coordinate(2729, 3348, 0));


        // -- TODO: WIP Diary items -- //
        //Explorer's ring
        var dest = new Coordinate(3052, 3292, 0);
        either(results, LUMBRIDGE, MEDIUM, "Explorer's ring", 5, TELEPORT, dest);
        either(results, LUMBRIDGE, HARD, "Explorer's ring", -1, TELEPORT, dest, 1f);
        either(results, LUMBRIDGE, ELITE, "Explorer's ring", -1, TELEPORT, dest, 1f);

        //Ardy cloak
        final var monastery = Pattern.compile("(Kandarin Monastery|Monastery Teleport)");
        dest = new Coordinate(2606, 3219, 0);
        for (var diff : DiaryRequirement.Difficulty.values()) {
            either(results, ARDOUGNE, diff, "Ardougne cloak", -1, monastery, dest, 1f);
        }

        final var farm = Pattern.compile("(Farm Teleport|Ardougne Farm)");
        dest = new Coordinate(2664, 3375, 0);
        either(results, ARDOUGNE, MEDIUM, "Ardougne cloak", 3, farm, dest);
        either(results, ARDOUGNE, HARD, "Ardougne cloak", 5, farm, dest);
        either(results, ARDOUGNE, ELITE, "Ardougne cloak", -1, farm, dest, 1f);

        //Fremmy boots
        dest = new Coordinate(2644, 3673, 0);
        either(results, FREMENNIK, EASY, "Fremennik sea boots", 1, TELEPORT, dest);
        either(results, FREMENNIK, MEDIUM, "Fremennik sea boots", 1, TELEPORT, dest);
        either(results, FREMENNIK, HARD, "Fremennik sea boots", 1, TELEPORT, dest);
        either(results, FREMENNIK, ELITE, "Fremennik sea boots", -1, TELEPORT, dest, 1f);

        //Rada's blessing
        dest = new Coordinate(1554, 3455, 0);
        final var kourendWoodland = Pattern.compile("Kourend Woodland");
        either(results, KOUREND_AND_KEBOS, EASY, "Rada's blessing", 3, kourendWoodland, dest);
        either(results, KOUREND_AND_KEBOS, MEDIUM, "Rada's blessing", 5, kourendWoodland, dest);
        either(results, KOUREND_AND_KEBOS, HARD, "Rada's blessing", -1, kourendWoodland, dest, 1f);
        either(results, KOUREND_AND_KEBOS, ELITE, "Rada's blessing", -1, kourendWoodland, dest, 1f);

        dest = new Coordinate(1311, 3800, 0);
        final var mtKaruulm = Pattern.compile("Mount Karuulm");
        either(results, KOUREND_AND_KEBOS, HARD, "Rada's blessing", 3, mtKaruulm, dest);
        either(results, KOUREND_AND_KEBOS, ELITE, "Rada's blessing", -1, mtKaruulm, dest, 1f);

        dest = new Coordinate(2898, 3709, 0);
        either(results, Pattern.compile("Ghommal's hilt [3-6]"), Pattern.compile("Trollheim"), dest);

        //Icy basalt
        results.add(inventory(Pattern.compile("Icy basalt"), Pattern.compile("Weiss"), new Coordinate(2846, 3940, 0)));

        return results;
    }

    private List<DialogItemTransport> loadDialogItemTransports() {
        final List<DialogItemTransport> results = new ArrayList<>();
        results.addAll(ChargedItem.getInventoryTransports());
        results.addAll(HousePortal.constructionCapeTransports());
        return results;
    }

    private void either(
        List<BasicItemTransport> transports,
        Pattern itemName,
        Pattern itemAction,
        Coordinate destination
    ) {
        transports.add(inventory(itemName, itemAction, destination));
        transports.add(equipped(itemName, itemAction, destination));
    }

    private BasicItemTransport inventory(Pattern itemName, Pattern itemAction, Coordinate destination) {
        return BasicItemTransport.builder()
            .itemName(itemName)
            .itemAction(itemAction)
            .destination(destination)
            .origin(BasicItemTransport.Origin.INVENTORY)
            .requirement(new ItemRequirement(itemName, 1))
            .cost(5f)
            .build();
    }

    private BasicItemTransport equipped(Pattern itemName, Pattern itemAction, Coordinate destination) {
        return BasicItemTransport.builder()
            .itemName(itemName)
            .itemAction(itemAction)
            .destination(destination)
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .requirement(new EquipmentRequirement(itemName))
            .cost(5f)
            .build();
    }

    private void either(
        List<BasicItemTransport> transports,
        DiaryRequirement.Region region,
        DiaryRequirement.Difficulty difficulty,
        String name,
        int limit,
        Pattern action,
        Coordinate destination,
        float cost
    ) {
        transports.add(inventory(region, difficulty, name, limit, action, destination, cost));
        transports.add(equipped(region, difficulty, name, limit, action, destination, cost));
    }

    private void either(
        List<BasicItemTransport> transports,
        DiaryRequirement.Region region,
        DiaryRequirement.Difficulty difficulty,
        String name,
        int limit,
        Pattern action,
        Coordinate destination
    ) {
        transports.add(inventory(region, difficulty, name, limit, action, destination, 5f));
        transports.add(equipped(region, difficulty, name, limit, action, destination, 5f));
    }

    private BasicItemTransport equipped(
        DiaryRequirement.Region region,
        DiaryRequirement.Difficulty difficulty,
        String name,
        int limit,
        Pattern action,
        Coordinate destination,
        float cost
    ) {
        final var pattern = Pattern.compile(name + " " + (difficulty.ordinal() + 1));
        return BasicItemTransport.builder()
            .itemName(pattern)
            .itemAction(action)
            .destination(destination)
            .origin(BasicItemTransport.Origin.EQUIPMENT)
            .cost(cost)
            .requirement(new TeleportsEnabled().and(new EquipmentRequirement(pattern)).and(new DailyLimitRequirement(region, limit)))
            .build();
    }

    private BasicItemTransport inventory(
        DiaryRequirement.Region region,
        DiaryRequirement.Difficulty difficulty,
        String name,
        int limit,
        Pattern action,
        Coordinate destination,
        float cost
    ) {
        final var pattern = Pattern.compile(name + " " + (difficulty.ordinal() + 1));
        return BasicItemTransport.builder()
            .itemName(pattern)
            .itemAction(action)
            .destination(destination)
            .origin(BasicItemTransport.Origin.INVENTORY)
            .cost(cost)
            .requirement(new TeleportsEnabled().and(new ItemRequirement(pattern, 1)).and(new DailyLimitRequirement(region, limit)))
            .build();
    }
}
