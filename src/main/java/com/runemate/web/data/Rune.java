package com.runemate.web.data;

import lombok.*;

@Getter
@RequiredArgsConstructor
public enum Rune {
    AIR(1),
    WATER(2),
    EARTH(3),
    FIRE(4),
    MIND(5),
    CHAOS(6),
    DEATH(7),
    BLOOD(8),
    COSMIC(9),
    NATURE(10),
    LAW(11),
    BODY(12),
    SOUL(13),
    ASTRAL(14),
    MIST(15),
    MUD(16),
    DUST(17),
    LAVA(18),
    STEAM(19),
    SMOKE(20),
    WRATH(21);

    private final int type;

    public static Rune of(int type) {
        for (final var rune : values()) {
            if (rune.type == type) {
                return rune;
            }
        }
        return null;
    }
}
