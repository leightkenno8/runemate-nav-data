package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;

@UtilityClass
public class Npcs {

    public Collection<NpcTransport> getAll() {
        final var results = new ArrayList<NpcTransport>();

        //Lumbridge Cavern - The Lost Tribe
        results.add(npc(
            "Kazgar",
            "Mines",
            new Coordinate(3229, 9610, 0),
            new Coordinate(3316, 9613, 0),
            10f,
            new QuestRequirement("THE_LOST_TRIBE", QuestRequirement.State.COMPLETE)
        ));
        results.add(npc(
            "Mistag",
            "Cellar",
            new Coordinate(3316, 9613, 0),
            new Coordinate(3229, 9610, 0),
            10f,
            new QuestRequirement("THE_LOST_TRIBE", QuestRequirement.State.COMPLETE)
        ));

        //Tree Gnome Village - Follow Elkoy
        results.add(npc(
            "Elkoy",
            "Follow",
            new Coordinate(2504, 3192, 0),
            new Coordinate(2515, 3159, 0),
            10f,
            new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.NOT_STARTED).not()
        ));
        results.add(npc(
            "Elkoy",
            "Follow",
            new Coordinate(2515, 3159, 0),
            new Coordinate(2504, 3192, 0),
            10f,
            new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.NOT_STARTED).not()
        ));

        //Jarvald - Waterbirth Island <> Relleka
        results.add(npc(
            "Jarvald",
            "Rellekka",
            new Coordinate(2544, 3760, 0),
            new Coordinate(2620, 3682, 0),
            10f,
            new QuestRequirement("THE_FREMENNIK_TRIALS", QuestRequirement.State.COMPLETE).or(new GoldRequirement(1000))
        ));
        results.add(npc(
            "Jarvald",
            "Waterbirth Island",
            new Coordinate(2620, 3682, 0),
            new Coordinate(2547, 3759, 0),
            10f,
            new QuestRequirement("THE_FREMENNIK_TRIALS", QuestRequirement.State.COMPLETE).or(new GoldRequirement(1000))
        ));

        //Entrana
        results.add(npc(
            "Monk of Entrana",
            "Take-boat",
            new Coordinate(3041, 3237, 0),
            new Coordinate(2834, 3331, 1),
            10f,
            Requirements.none()
        ));
        results.add(npc(
            "Monk of Entrana",
            "Take-boat",
            new Coordinate(2834, 3335, 0),
            new Coordinate(3048, 3231, 1),
            10f,
            Requirements.none()
        ));

        //Fossil Island
        results.add(npc(
            "Barge guard",
            "Quick-travel",
            new Coordinate(3362, 3445, 0),
            new Coordinate(3724, 3808, 0),
            10f,
            new QuestRequirement("BONE_VOYAGE", QuestRequirement.State.COMPLETE)
        ));

        //Crabclaw Isle
        results.add(npc(
            "Sandicrahb",
            "Travel",
            new Coordinate(1782, 3458, 0),
            new Coordinate(1778, 3417, 0),
            10f,
            new GoldRequirement(10000).and(new MembersRequirement(true))
        ));
        results.add(npc(
            "Sandicrahb",
            "Travel",
            new Coordinate(1779, 3418, 0),
            new Coordinate(1784, 3458, 0),
            10f,
            new MembersRequirement(true)
        ));

        //Port Sarim
        //Cabin boy herbert
        final var aKingdomDivided = new QuestRequirement("A_KINGDOM_DIVIDED", QuestRequirement.State.COMPLETE);

        //From Port Sarim
        results.add(npc("Veos", "Land's End", new Coordinate(3053, 3245, 0), new Coordinate(1504, 3996, 1), 5f, aKingdomDivided.not().and(new MembersRequirement(true))));
        results.add(npc("Veos", "Port Piscarilius", new Coordinate(3053, 3245, 0), new Coordinate(1824, 3695, 1), 5f, aKingdomDivided.not().and(new MembersRequirement(true))));
        results.add(npc("Cabin Boy Herbert", "Land's End", new Coordinate(3053, 3245, 0), new Coordinate(1504, 3995, 1), 5f, aKingdomDivided.and(new MembersRequirement(true))));
        results.add(npc("Cabin Boy Herbert", "Port Piscarilius", new Coordinate(3053, 3245, 0), new Coordinate(1824, 3694, 1), 5f, aKingdomDivided.and(new MembersRequirement(true))));

        //From Port Pisc
        results.add(npc("Veos", "Port Sarim", new Coordinate(1826, 3691, 0), new Coordinate(3056, 3242, 1), 5f, aKingdomDivided.not().and(new MembersRequirement(true))));
        results.add(npc("Veos", "Land's End", new Coordinate(1826, 3691, 0), new Coordinate(1504, 3996, 1), 5f, aKingdomDivided.not().and(new MembersRequirement(true))));
        results.add(npc("Cabin Boy Herbert", "Port Sarim", new Coordinate(1826, 3691, 0), new Coordinate(3055, 3242, 1), 5f, aKingdomDivided.and(new MembersRequirement(true))));
        results.add(npc("Cabin Boy Herbert", "Land's End", new Coordinate(1826, 3691, 0), new Coordinate(1504, 3995, 1), 5f, aKingdomDivided.and(new MembersRequirement(true))));

        results.add(npc("Captain Magoro", "Port Sarim", new Coordinate(1504, 3401, 0), new Coordinate(3055, 3242, 1), 5f, new MembersRequirement(true)));
        results.add(npc("Captain Magoro", "Port Piscarilius", new Coordinate(1504, 3401, 0), new Coordinate(1824, 3695, 1), 5f, new MembersRequirement(true)));

        //From Brimhaven
        results.add(npc("Captain Barnaby", "Ardougne", new Coordinate(2772, 3229, 0), new Coordinate(2683, 3268, 1), 10f, new MembersRequirement(true).and(new GoldRequirement(30))));
        results.add(npc("Captain Barnaby", "Rimmington", new Coordinate(2772, 3229, 0), new Coordinate(2915, 3221, 1), 10f, new MembersRequirement(true).and(new GoldRequirement(30))));

        //From Rimmington
        results.add(npc("Captain Barnaby", "Ardougne", new Coordinate(2915, 3226, 0), new Coordinate(2683, 3268, 1), 5f, new MembersRequirement(true).and(new GoldRequirement(30))));
        results.add(npc("Captain Barnaby", "Brimhaven", new Coordinate(2915, 3226, 0), new Coordinate(2775, 3233, 1), 5f, new MembersRequirement(true).and(new GoldRequirement(30))));

        //From Ardougne
        results.add(npc("Captain Barnaby", "Brimhaven", new Coordinate(2678, 3275, 0), new Coordinate(2775, 3233, 1), 10f, new MembersRequirement(true).and(new GoldRequirement(30))));
        results.add(npc("Captain Barnaby", "Rimmington", new Coordinate(2678, 3275, 0), new Coordinate(2915, 3221, 1), 10f, new MembersRequirement(true).and(new GoldRequirement(30))));

        //Torfinn
        results.add(npc("Torfinn", "Ungael", new Coordinate(2641, 3696, 0), new Coordinate(2277, 4034, 0), 5f, new MembersRequirement(true)));
        results.add(npc("Torfinn", "Rellekka", new Coordinate(2277, 4034, 0), new Coordinate(2641, 3696, 0), 5f, new MembersRequirement(true)));

        //Catalytic Guardian -> Leviathan
        results.add(npc("Catalytic Guardian", "Travel", new Coordinate(3611, 9472, 0), new Coordinate(2012, 6435, 0), 5f, QuestRequirement.complete("DESERT_TREASURE_II")));
        return results;
    }

    private NpcTransport npc(
        String name, String action, Coordinate position, Coordinate destination, float wanderRadius, Requirement requirement
    ) {
        return NpcTransport.builder()
            .npcName(name)
            .npcAction(action)
            .source(position)
            .wanderRadius(wanderRadius)
            .destination(destination)
            .requirement(requirement).build();
    }

    private NpcTransport dialogNpc(
        String name, String action, Pattern dialog, Coordinate position, Coordinate destination, float wanderRadius, Requirement requirement
    ) {
        return DialogNpcTransport.builder()
            .npcName(name)
            .npcAction(action)
            .source(position)
            .destination(destination)
            .wanderRadius(wanderRadius)
            .dialogPattern(dialog)
            .requirement(requirement).build();
    }

}
