package com.runemate.web.data;

import com.runemate.web.model.Coordinate;
import com.runemate.web.transport.fixed.FixedTransport;
import com.runemate.web.transport.fixed.GameObjectTransport;
import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Collection;

/**
 * The web doesn't currently consider diagonal movement when generating the graph.
 * This means that objects which would get detected and linked by heuristics are overlooked if they are diagonal.
 * This is a workaround to allow for navigation through these objects.
 */
@UtilityClass
public class DiagonalObjects {
    public Collection<FixedTransport> getAll() {
        return Arrays.asList(
                //Wizard's tower - South-western door which blocks the stairs and the ladder
                GameObjectTransport.builder()
                        .source(new Coordinate(3106, 3161, 0))
                        .destination(new Coordinate(3108, 3163, 0 ))
                        .objectPosition(new Coordinate(3107, 3162, 0))
                        .objectName("Door")
                        .objectAction("Open")
                        .build()
        );
    }
}
