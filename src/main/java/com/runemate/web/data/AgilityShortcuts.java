package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;

/**
 * Basic agility shortcuts that aren't covered automatically
 */
@UtilityClass
public class AgilityShortcuts {

    public Collection<FixedTransport> getAll() {
        return Arrays.asList(
            //"Mos Le'Harmless - Stepping Stone"
            GameObjectTransport.builder()
                .source(new Coordinate(3708, 2969, 0))
                .destination(new Coordinate(3714, 2969, 0))
                .objectName("Stepping stone")
                .objectAction("Jump-to")
                .objectPosition(new Coordinate(3711, 2969, 0))
                .requirement(SkillRequirement.Skill.AGILITY.required(60))
                .bidirectional(true)
                .build(),

            //Rev caves obstacles
            GameObjectTransport.builder()
                .source(new Coordinate(3220, 10084, 0))
                .objectPosition(new Coordinate(3220, 10086, 0))
                .destination(new Coordinate(3220, 10088, 0))
                .objectName("Pillar")
                .objectAction("Jump-to")
                .requirement(SkillRequirement.Skill.AGILITY.required(65))
                .bidirectional(true)
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(3198, 10136, 0))
                .objectPosition(new Coordinate(3200, 10136, 0))
                .destination(new Coordinate(3202, 10136, 0))
                .objectName("Pillar")
                .objectAction("Jump-to")
                .requirement(SkillRequirement.Skill.AGILITY.required(75))
                .bidirectional(true)
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(3239, 10145, 0))
                .objectPosition(new Coordinate(3241, 10145, 0))
                .destination(new Coordinate(3243, 10145, 0))
                .objectName("Pillar")
                .objectAction("Jump-to")
                .requirement(SkillRequirement.Skill.AGILITY.required(89))
                .bidirectional(true)
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(3200, 10196, 0))
                .objectPosition(new Coordinate(3202, 10196, 0))
                .destination(new Coordinate(3204, 10196, 0))
                .objectName("Pillar")
                .objectAction("Jump-to")
                .requirement(SkillRequirement.Skill.AGILITY.required(75))
                .bidirectional(true)
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(3180, 10207, 0))
                .objectPosition(new Coordinate(3180, 10209, 0))
                .destination(new Coordinate(3180, 10211, 0))
                .objectName("Pillar")
                .objectAction("Jump-to")
                .requirement(SkillRequirement.Skill.AGILITY.required(75))
                .bidirectional(true)
                .build(),

            //Fossil island "cave" shortcuts
            GameObjectTransport.builder()
                .source(new Coordinate(3713, 3830, 0))
                .objectPosition(new Coordinate(3712, 3828, 0))
                .destination(new Coordinate(3715, 3815, 0))
                .objectName("Hole")
                .objectAction("Climb through")
                .requirement(SkillRequirement.Skill.AGILITY.required(70))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(3715, 3815, 0))
                .objectPosition(new Coordinate(3714, 3816, 0))
                .destination(new Coordinate(3713, 3830, 0))
                .objectName("Hole")
                .objectAction("Climb through")
                .requirement(SkillRequirement.Skill.AGILITY.required(70))
                .build(),

            //Zulandara stepping stones
            GameObjectTransport.builder()
                .source(new Coordinate(2154, 3072, 0))
                .objectPosition(new Coordinate(2157, 3072, 0))
                .destination(new Coordinate(2160, 3072, 0))
                .objectName("Stepping stone")
                .objectAction("Cross")
                .requirement(SkillRequirement.Skill.AGILITY.required(76))
                .build(),

            //Vorkath's lair entrance
            GameObjectTransport.builder()
                .source(new Coordinate(2272, 4052, 0))
                .objectPosition(new Coordinate(2272, 4053, 0))
                .destination(new Coordinate(2272, 4054, 0))
                .objectAction("Climb-over")
                .objectName("Ice chunks")
                .bidirectional(true)
                .build(),

            //Trollheim top
            GameObjectTransport.builder()
                .source(new Coordinate(2886, 3683, 0))
                .objectPosition(new Coordinate(2885, 3683, 0))
                .destination(new Coordinate(2884, 3683, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .bidirectional(true)
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 43))
                .build(),

            //Trollheim middle
            GameObjectTransport.builder()
                .source(new Coordinate(2878, 3668, 0))
                .objectPosition(new Coordinate(2878, 3667, 0))
                .destination(new Coordinate(2878, 3665, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 43))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2878, 3665, 0))
                .objectPosition(new Coordinate(2878, 3666, 0))
                .destination(new Coordinate(2878, 3668, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 43))
                .build(),

            //Trollheim bottom
            GameObjectTransport.builder()
                .source(new Coordinate(2872, 3671, 0))
                .objectPosition(new Coordinate(2871, 3671, 0))
                .destination(new Coordinate(2869, 3671, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 41))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2869, 3671, 0))
                .objectPosition(new Coordinate(2870, 3671, 0))
                .destination(new Coordinate(2872, 3671, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 41))
                .build(),

            //GWD Crack
            GameObjectTransport.builder()
                .source(new Coordinate(2899, 3713, 0))
                .objectPosition(new Coordinate(2900, 3713, 0))
                .destination(new Coordinate(2902, 3721, 0))
                .objectAction("Crawl-through")
                .objectName("Little crack")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70))
                .build(),

            //GWD Boulder
            GameObjectTransport.builder()
                .source(new Coordinate(2898, 3711, 0))
                .objectPosition(new Coordinate(2898, 3716, 0))
                .destination(new Coordinate(2902, 3721, 0))
                .objectName("Boulder")
                .objectAction("Move")
                .cost(50f)
                .requirement(new SkillRequirement(SkillRequirement.Skill.STRENGTH, 70))
                .build(),

            //GWD Hole
            GameObjectTransport.builder()
                .source(new Coordinate(2916, 3746, 0))
                .objectPosition(new Coordinate(2917, 3745, 0))
                .destination(new Coordinate(2882, 5311, 2))
                .objectName("Hole")
                .objectAction("Climb-down")
                .requirement(new SkillRequirement(SkillRequirement.Skill.STRENGTH, 70))
                .build(),

            //Bandos GWD wing
            GameObjectTransport.builder()
                .source(new Coordinate(2851, 5333, 2))
                .objectPosition(new Coordinate(2851, 5333, 2))
                .destination(new Coordinate(2850, 5333, 2))
                .objectName("Big door")
                .objectAction("Open")
                .requirement(new ItemRequirement(Pattern.compile("Hammer"), 1))
                .bidirectional(true)
                .build(),

            //Zamorak GWD wing
            GameObjectTransport.builder()
                .source(new Coordinate(2885, 5329, 2))
                .objectPosition(new Coordinate(2885, 5333, 2))
                .destination(new Coordinate(2885, 5348, 2))
                .objectAction("Climb-off")
                .objectName("Ice bridge")
                .requirement(new SkillRequirement(SkillRequirement.Skill.CONSTITUTION, 70))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2885, 5348, 2))
                .objectPosition(new Coordinate(2885, 5344, 2))
                .destination(new Coordinate(2885, 5329, 2))
                .objectAction("Climb-off")
                .objectName("Ice bridge")
                .requirement(new SkillRequirement(SkillRequirement.Skill.CONSTITUTION, 70))
                .build(),

            //Saradomin GWD wing
            GameObjectTransport.builder()
                .source(new Coordinate(2912, 5300, 2))
                .objectPosition(new Coordinate(2913, 5300, 2))
                .destination(new Coordinate(2915, 5300, 1))
                .objectAction("Climb-down")
                .objectName("Rock")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2920, 5276, 1))
                .objectPosition(new Coordinate(2920, 5275, 1))
                .destination(new Coordinate(2918, 5273, 0))
                .objectAction("Climb-down")
                .objectName("Rock")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2914, 5300, 1))
                .objectPosition(new Coordinate(2914, 5300, 1))
                .destination(new Coordinate(2912, 5299, 2))
                .objectAction("Climb-up")
                .objectName("Rope")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2919, 5274, 0))
                .objectPosition(new Coordinate(2920, 5274, 0))
                .destination(new Coordinate(2919, 5276, 1))
                .objectAction("Climb-up")
                .objectName("Rope")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70))
                .build(),

            //Stronghold Slayer Cave
            GameObjectTransport.builder()
                .source(new Coordinate(2429, 9807, 0))
                .objectPosition(new Coordinate(2430, 9807, 0))
                .destination(new Coordinate(2435, 9807, 0))
                .objectAction("Enter")
                .objectName("Tunnel")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 72))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2435, 9807, 0))
                .objectPosition(new Coordinate(2434, 9807, 0))
                .destination(new Coordinate(2429, 9807, 0))
                .objectAction("Enter")
                .objectName("Tunnel")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 72))
                .build(),

            //Weiss shortcuts
            GameObjectTransport.builder()
                .source(new Coordinate(2852, 3936, 0))
                .objectPosition(new Coordinate(2851, 3936, 0))
                .destination(new Coordinate(2850, 3936, 0))
                .objectAction("Cross")
                .objectName("Little boulder")
                .bidirectional(true)
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2865, 3938, 0))
                .objectPosition(new Coordinate(2866, 3938, 0))
                .destination(new Coordinate(2866, 3938, 0))
                .objectAction("Squeeze-through")
                .objectName("Broken fence")
                .bidirectional(true)
                .build(),

            //Zeah RC agility area
            GameObjectTransport.builder()
                .source(new Coordinate(1742, 3854, 0))
                .objectPosition(new Coordinate(1743, 3854, 0))
                .destination(new Coordinate(1752, 3854, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 73))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(1776, 3884, 0))
                .objectPosition(new Coordinate(1776, 3883, 0))
                .destination(new Coordinate(1776, 3880, 0))
                .objectAction("Jump")
                .objectName("Boulder")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 49))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(1774, 3849, 0))
                .objectPosition(new Coordinate(1773, 3849, 0))
                .destination(new Coordinate(1769, 3849, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 52))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(1769, 3849, 0))
                .objectPosition(new Coordinate(1770, 3849, 0))
                .destination(new Coordinate(1774, 3849, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 52))
                .build(),

            //Mount Karuluum Lower shortcut - Ascending
            GameObjectTransport.builder()
                .source(new Coordinate(1324, 3777, 0))
                .objectPosition(new Coordinate(1324, 3778, 0))
                .destination(new Coordinate(1324, 3785, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 29))
                .build(),
            //Mount Karuluum Lower shortcut - Descending
            GameObjectTransport.builder()
                .source(new Coordinate(1324, 3785, 0))
                .objectPosition(new Coordinate(1324, 3784, 0))
                .destination(new Coordinate(1324, 3777, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 29))
                .build(),

            //Mount Karuluum Upper shortcut - Ascending
            GameObjectTransport.builder()
                .source(new Coordinate(1324, 3787, 0))
                .objectPosition(new Coordinate(1324, 3788, 0))
                .destination(new Coordinate(1324, 3795, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 62))
                .build(),
            //Mount Karuluum Upper shortcut - Descending
            GameObjectTransport.builder()
                .source(new Coordinate(1324, 3795, 0))
                .objectPosition(new Coordinate(1324, 3794, 0))
                .destination(new Coordinate(1324, 3787, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 62))
                .build(),

            //Alchemical hydra shortcut
            GameObjectTransport.builder()
                .source(new Coordinate(1316, 10213, 0))
                .objectPosition(new Coordinate(1316, 10214, 0))
                .destination(new Coordinate(1346, 10232, 0))
                .objectAction("Enter")
                .objectName("Mysterious pipe")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 88))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(1346, 10232, 0))
                .objectPosition(new Coordinate(1346, 10231, 0))
                .destination(new Coordinate(1316, 10213, 0))
                .objectAction("Enter")
                .objectName("Mysterious pipe")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 88))
                .build(),

            //Armadyl's Eyrie
            GameObjectTransport.builder()
                .source(new Coordinate(2872, 5280, 2))
                .objectPosition(new Coordinate(2871, 5270, 2))
                .destination(new Coordinate(2872, 5268, 2))
                .objectAction("Grapple")
                .objectName("Pillar")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 70)
                    .and(new EquipmentRequirement("Mith grapple"))
                    .and(new EquipmentRequirement(Pattern.compile(".+ crossbow"))))
                .bidirectional(true)
                .build(),

            //Lassar rocks to Camdozaal
            GameObjectTransport.builder()
                .source(new Coordinate(3002, 3483, 0))
                .objectPosition(new Coordinate(3001, 3483, 0))
                .destination(new Coordinate(2997, 3483, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 68))
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2997, 3483, 0))
                .objectPosition(new Coordinate(2998, 3483, 0))
                .destination(new Coordinate(3002, 3483, 0))
                .objectAction("Climb")
                .objectName("Rocks")
                .requirement(new SkillRequirement(SkillRequirement.Skill.AGILITY, 68))
                .build(),

            //The scar stepping stones
            GameObjectTransport.builder()
                .source(new Coordinate(2029, 6430, 0))
                .objectPosition(new Coordinate(2031, 6430, 0))
                .destination(new Coordinate(2035, 6430, 0))
                .objectAction("Cross")
                .objectName("Stepping Stone")
                .build(),
            GameObjectTransport.builder()
                .source(new Coordinate(2035, 6430, 0))
                .objectPosition(new Coordinate(2033, 6430, 0))
                .destination(new Coordinate(2029, 6430, 0))
                .objectAction("Cross")
                .objectName("Stepping Stone")
                .build()
        );
    }

}
