package com.runemate.web.data;

import com.runemate.web.*;
import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.dynamic.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;

public enum Spell {

    //Standard
    VARROCK_TELEPORT(new Coordinate(3212, 3424), 25, SpellbookRequirement.standard(), new RuneRequirement(Rune.AIR, 3), new RuneRequirement(Rune.FIRE, 1), new RuneRequirement(Rune.LAW, 1)),
    GRAND_EXCHANGE_TELEPORT(new Coordinate(3163, 3479), 25, SpellbookRequirement.standard(), new RuneRequirement(Rune.AIR, 3), new RuneRequirement(Rune.FIRE, 1), new RuneRequirement(Rune.LAW, 1), new DiaryRequirement(DiaryRequirement.Region.VARROCK, DiaryRequirement.Difficulty.MEDIUM)),
    LUMBRIDGE_TELEPORT(new Coordinate(3225, 3219), 31, SpellbookRequirement.standard(), new RuneRequirement(Rune.EARTH, 1), new RuneRequirement(Rune.AIR, 3), new RuneRequirement(Rune.LAW, 1)),
    FALADOR_TELEPORT(new Coordinate(2966, 3379), 37, SpellbookRequirement.standard(), new RuneRequirement(Rune.WATER, 1), new RuneRequirement(Rune.AIR, 3), new RuneRequirement(Rune.LAW, 1)),
    CAMELOT_TELEPORT(new Coordinate(2757, 3479), 45, SpellbookRequirement.standard(), new RuneRequirement(Rune.AIR, 5), new RuneRequirement(Rune.LAW, 1)),
    SEERS_TELEPORT(new Coordinate(2726, 3485), 45, SpellbookRequirement.standard(), new RuneRequirement(Rune.AIR, 5), new RuneRequirement(Rune.LAW, 1), new DiaryRequirement(DiaryRequirement.Region.KANDARIN, DiaryRequirement.Difficulty.HARD)),
    ARDOUGNE_TELEPORT(new Coordinate(2661, 3300), 51, SpellbookRequirement.standard(), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.WATER, 2), QuestRequirement.complete("PLAGUE_CITY")),
    WATCHTOWER_TELEPORT(new Coordinate(2931, 4717, 2), 58, SpellbookRequirement.standard(), new RuneRequirement(Rune.EARTH, 2), new RuneRequirement(Rune.LAW, 2), QuestRequirement.complete("WATCHTOWER")),
    YANILLE_TELEPORT(new Coordinate(2583, 3097), 58, SpellbookRequirement.standard(), new RuneRequirement(Rune.EARTH, 2), new RuneRequirement(Rune.LAW, 2), QuestRequirement.complete("WATCHTOWER"), new DiaryRequirement(DiaryRequirement.Region.ARDOUGNE, DiaryRequirement.Difficulty.HARD)),
    TROLLHEIM_TELEPORT(new Coordinate(2888, 3674), 61, SpellbookRequirement.standard(), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.FIRE, 2), QuestRequirement.complete("EADGARS_RUSE")),
//    TELEPORT_TO_APE_ATOLL(new Coordinate(2796, 2791), 64, SpellbookRequirement.standard()),
    KOUREND_CASTLE_TELEPORT(new Coordinate(1643, 3673), 69, SpellbookRequirement.standard(), new RuneRequirement(Rune.FIRE, 5), new RuneRequirement(Rune.WATER, 4), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 2)),

    //Ancient
    PADDEWWA_TELEPORT(new Coordinate(3098, 9883), 54, SpellbookRequirement.ancient(), new RuneRequirement(Rune.AIR, 1), new RuneRequirement(Rune.FIRE, 1), new RuneRequirement(Rune.LAW, 2)),
    SENNTISTEN_TELEPORT(new Coordinate(3321, 3335), 60, SpellbookRequirement.ancient(), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 1)),
    KHARYRLL_TELEPORT(new Coordinate(3493, 3474), 66, SpellbookRequirement.ancient(), new RuneRequirement(Rune.BLOOD, 1), new RuneRequirement(Rune.LAW, 2)),
    LASSAR_TELEPORT(new Coordinate(3004, 3470), 72, SpellbookRequirement.ancient(), new RuneRequirement(Rune.WATER, 4), new RuneRequirement(Rune.LAW, 2)),
    DAREEYAK_TELEPORT(new Coordinate(2969, 3695), 78, SpellbookRequirement.ancient(), new RuneRequirement(Rune.AIR, 2), new RuneRequirement(Rune.FIRE, 3), new RuneRequirement(Rune.LAW, 2)),
    CARRALLANGAR_TELEPORT(new Coordinate(3157, 3667), 84, SpellbookRequirement.ancient(), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 2)),
    ANNAKARL_TELEPORT(new Coordinate(3288, 3888), 90, SpellbookRequirement.ancient(), new RuneRequirement(Rune.BLOOD, 2), new RuneRequirement(Rune.LAW, 2)),
    GHORROCK_TELEPORT(new Coordinate(2977, 3872), 96, SpellbookRequirement.ancient(), new RuneRequirement(Rune.WATER, 8), new RuneRequirement(Rune.LAW, 2)),

    //Lunar
    MOONCLAN_TELEPORT(new Coordinate(2113, 3917), 69, SpellbookRequirement.lunar(), new RuneRequirement(Rune.EARTH, 2), new RuneRequirement(Rune.ASTRAL, 2), new RuneRequirement(Rune.LAW, 1)),
    OURANIA_TELEPORT(new Coordinate(2470, 3247), 71, SpellbookRequirement.lunar(), new RuneRequirement(Rune.EARTH, 6), new RuneRequirement(Rune.ASTRAL, 2), new RuneRequirement(Rune.LAW, 1)),
    WATERBIRTH_TELEPORT(new Coordinate(2548, 3758), 72, SpellbookRequirement.lunar(), new RuneRequirement(Rune.WATER, 1), new RuneRequirement(Rune.ASTRAL, 2), new RuneRequirement(Rune.LAW, 1)),
    BARBARIAN_TELEPORT(new Coordinate(2545, 3571), 75, SpellbookRequirement.lunar(), new RuneRequirement(Rune.FIRE, 3), new RuneRequirement(Rune.ASTRAL, 2), new RuneRequirement(Rune.LAW, 2)),
    KHAZARD_TELEPORT(new Coordinate(2637, 3168), 78, SpellbookRequirement.lunar(), new RuneRequirement(Rune.WATER, 4), new RuneRequirement(Rune.ASTRAL, 2), new RuneRequirement(Rune.LAW, 2)),
    FISHING_GUILD_TELEPORT(new Coordinate(2610, 3389), 85, SpellbookRequirement.lunar(), new RuneRequirement(Rune.WATER, 10), new RuneRequirement(Rune.ASTRAL, 3), new RuneRequirement(Rune.LAW, 3)),
    CATHERBY_TELEPORT(new Coordinate(2802, 3449), 87, SpellbookRequirement.lunar(), new RuneRequirement(Rune.WATER, 10), new RuneRequirement(Rune.ASTRAL, 3), new RuneRequirement(Rune.LAW, 3)),
    ICE_PLATEAU_TELEPORT(new Coordinate(2973, 3939), 89, SpellbookRequirement.lunar(), new RuneRequirement(Rune.WATER, 8), new RuneRequirement(Rune.ASTRAL, 3), new RuneRequirement(Rune.LAW, 3)),

    //Arceuus
    ARCEUUS_LIBRARY_TELEPORT(new Coordinate(1634, 3836), 6, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.EARTH, 2), new RuneRequirement(Rune.LAW, 1)),
    DRAYNOR_MANOR_TELEPORT(new Coordinate(3109, 3352), 17, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.EARTH, 1), new RuneRequirement(Rune.WATER, 1), new RuneRequirement(Rune.LAW, 1)),
    BATTLEFRONT_TELEPORT(new Coordinate(1350, 3740), 23, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.EARTH, 1), new RuneRequirement(Rune.FIRE, 1), new RuneRequirement(Rune.LAW, 1)),
    MIND_ALTAR_TELEPORT(new Coordinate(2978, 3508), 28, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.LAW, 1), new RuneRequirement(Rune.MIND, 2)),
    SALVE_GRAVEYARD_TELEPORT(new Coordinate(3431, 3460), 40, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.LAW, 1), new RuneRequirement(Rune.SOUL, 2), QuestRequirement.complete("PRIEST_IN_PERIL")),
    FENKENSTRAINS_CASTLE_TELEPORT(new Coordinate(3546, 3528), 48, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.EARTH, 1), new RuneRequirement(Rune.LAW, 1), new RuneRequirement(Rune.SOUL, 1), QuestRequirement.complete("PRIEST_IN_PERIL")),
    WEST_ARDOUGNE_TELEPORT(new Coordinate(2502, 3291), 61, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 2), QuestRequirement.complete("BIOHAZARD")),
    HARMONY_ISLAND_TELEPORT(new Coordinate(3799, 2867), 65, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.LAW, 1), new RuneRequirement(Rune.NATURE, 1), new RuneRequirement(Rune.SOUL, 1), QuestRequirement.complete("THE_GREAT_BRAIN_ROBBERY")),
    CEMETERY_TELEPORT(new Coordinate(2978, 3763), 71, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.BLOOD, 1), new RuneRequirement(Rune.LAW, 1), new RuneRequirement(Rune.SOUL, 1)),
    BARROWS_TELEPORT(new Coordinate(3563, 3313), 83, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.BLOOD, 1), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 2)),
    APE_ATOLL_TELEPORT(new Coordinate(2769, 9100), 90, SpellbookRequirement.arceuus(), new RuneRequirement(Rune.BLOOD, 2), new RuneRequirement(Rune.LAW, 2), new RuneRequirement(Rune.SOUL, 2)),

    //House portal teleports
    HOUSE_RIMMINGTON(HousePortal.RIMMINGTON) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_TAVERLY(HousePortal.TAVERLY) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_POLLNIVNEACH(HousePortal.POLLNIVNEACH) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_RELLEKKA(HousePortal.RELLEKKA) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_BRIMHAVEN(HousePortal.BRIMHAVEN) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_YANILLE(HousePortal.YANILLE) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_HOSIDIUS(HousePortal.HOSIDIUS) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    },
    HOUSE_PRIFDDINAS(HousePortal.PRIFDDINAS) {
        @Override
        public String spell() {
            return "TELEPORT_OUTSIDE_HOUSE";
        }
    };

    private final @Getter Coordinate destination;
    private final SpellbookRequirement spellbook;
    private final Requirement requirement;
    private final float cost;

    Spell(Coordinate destination, int level, SpellbookRequirement spellbook, Requirement... requirements) {
        this.destination = destination;
        this.spellbook = spellbook;
        this.requirement = Arrays.stream(requirements)
            .reduce(spellbook, Requirement::and)
            .and(new SkillRequirement(SkillRequirement.Skill.MAGIC, level))
            .and(new TeleportsEnabled());
        this.cost = requirements.length;
    }

    Spell(HousePortal destination) {
        this.destination = destination.getPosition();
        this.spellbook = SpellbookRequirement.standard();
        this.requirement = Requirements.and(
            new RuneRequirement(Rune.LAW, 1),
            new RuneRequirement(Rune.EARTH, 1),
            new RuneRequirement(Rune.AIR, 1),
            new SkillRequirement(SkillRequirement.Skill.MAGIC, 40),
            destination.isHome(),
            new TeleportsEnabled()
        );
        this.cost = 3f;
    }

    public String spell() {
        return name();
    }
    
    public DynamicTransport asTransport() {
        return SpellTransport.builder()
            .spell(spell())
            .spellbook(spellbook.getSpellbook())
            .requirement(requirement)
            .destination(destination)
            .cost(cost)
            .build();
    }

    public static List<DynamicTransport> getAll() {
        return Arrays.stream(values()).map(Spell::asTransport).collect(Collectors.toList());
    }

    public static List<DynamicTransport> getAll(@NonNull WebContext context) {
        return Arrays.stream(values())
            .filter(s -> s.spellbook.satisfy(context) && s.requirement.satisfy(context))
            .map(Spell::asTransport)
            .collect(Collectors.toList());
    }

}
