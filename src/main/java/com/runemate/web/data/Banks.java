package com.runemate.web.data;

import com.runemate.web.WebContext;
import com.runemate.web.model.Area;
import com.runemate.web.util.IntOp;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Bank locations, for use in landmark traversal
 */
public class Banks {
    @NonNull
    @Getter(lazy = true)
    private static final List<Area> banks = Arrays.stream(BankArea.values()).parallel().filter(bank -> bank.requirement == null).map(BankArea::getArea).collect(Collectors.toUnmodifiableList());

    @NonNull
    public static List<Area> getBanks(WebContext context) {
        return Arrays.stream(BankArea.values()).parallel().filter(area -> area.requirement == null || area.requirement.test(context)).map(BankArea::getArea).collect(Collectors.toUnmodifiableList());
    }

    @Getter
    @RequiredArgsConstructor
    @AllArgsConstructor
    private enum BankArea {
        //	ETCETERIA_BANK(new Area(2618, 3893, 4, 4, 0)), has quest requirements
        //	HALLOWED_SEPULCHRE_BANK(new Area(2393, 5975, 15, 15, 0)), has quest requirements
        //	MOTHERLODE_MINE_BANK(new Area(3754, 5664, 4, 3, 0)), has pickaxe requirement
//        DARKMEYER_BANK(new Area(3601, 3365, 9, 3, 0)), //DANGEROUS
        AL_KHARID_BANK(new Area(3269, 3161, 4, 13, 0)),
        ARCEUUS_BANK(new Area(1625, 3741, 7, 7, 0)),
        ARDOUGNE_NORTH_BANK(new Area(2612, 3330, 10, 6, 0)),
        ARDOUGNE_SOUTH_BANK(new Area(2649, 3280, 7, 8, 0)),
        BARBARIAN_OUTPOST_BANK(new Area(2532, 3570, 6, 10, 0)),
        BURGH_DE_ROTT_BANK(new Area(3492, 3208, 10, 6, 0)),
        CANIFIS_BANK(new Area(3508, 3474, 6, 10, 0)),
        CASTLE_WARS_BANK(new Area(2435, 3081, 12, 18, 0)),
        CATHERBY_BANK(new Area(2806, 3438, 7, 4, 0)),
        CHARCOAL_BURNERS_BANK(new Area(1711, 3460, 14, 10, 0)),
        CORSAIR_COVE_BANK(new Area(2567, 2862, 7, 7, 0)),
        DRAYNOR_BANK(new Area(3092, 3240, 6, 7, 0)),
        DUEL_ARENA_BANK(new Area(3380, 3267, 5, 7, 0)),
        EDGEVILLE_BANK(new Area(3091, 3488, 8, 12, 0)),
        FALADOR_EAST_BANK(new Area(3009, 3355, 13, 4, 0)),
        FALADOR_WEST_BANK(new Area(2943, 3368, 7, 6, 0)),
        FARMING_GUILD_BANK(new Area(1251, 3739, 5, 6, 0)),
        FEROX_ENCLAVE_BANK(new Area(3127, 3627, 10, 6, 0)),
        FOSSIL_ISLAND_BANK(new Area(3738, 3804, 2, 2, 0), ctx -> ctx.hasVarbit(5800, 1)),
        GRAND_EXCHANGE_BANK(new Area(3154, 3480, 22, 22, 0)),
        GRAND_TREE_SOUTH_BANK(new Area(2448, 3476, 8, 8, 1)),
        GRAND_TREE_WEST_BANK(new Area(2436, 3484, 9, 8, 1)),
        HOSIDIUS_BANK(new Area(1748, 3594, 5, 8, 0)),
        JATIZSO_BANK(new Area(2413, 3798, 7, 7, 0)),
        KARUULM_BANK(new Area(1322, 3823, 5, 3, 0)),
        LLETYA_BANK(new Area(2349, 3160, 8, 7, 0)),
        LOVAKENGJ_BANK(new Area(1518, 3735, 16, 8, 0)),
        LUMBRIDGE_BANK(new Area(3207, 3215, 4, 8, 2)),
        NEITIZNOT_BANK(new Area(2334, 3805, 6, 2, 0)),
        NARDAH_BANK(new Area(3424, 2889, 6, 5, 0)),
        PEST_CONTROL_BANK(new Area(2664, 2651, 4, 4, 0)),
        PORT_KHAZARD_BANK(new Area(2658, 3156, 7, 9, 0)),
        PORT_PISCARILIUS_BANK(new Area(1794, 3784, 18, 7, 0)),
        PRIFDDINAS_BANK(new Area(3255, 6105, 4, 4, 0)),
        RFD_CHEST(new Area(3215, 9620, 4, 4, 0), ctx -> ctx.hasVarbit(1850, 1, IntOp.GTE)),
        SEERS_VILLAGE_BANK(new Area(2721, 3487, 10, 7, 0)),
        SULFUR_MINE(new Area(1452, 3857, 4, 4, 0)),
        SHANTAY_PASS_BANK(new Area(3299, 3118, 11, 10, 0)),
        SHAYZIEN_BANK(new Area(1484, 3589, 7, 7, 0)),
        SHILO_VILLAGE_BANK(new Area(2842, 2951, 20, 8, 0)),
        TREE_GNOME_STRONGHOLD_BANK(new Area(2441, 3414, 11, 23, 1)),
        VARROCK_EAST_BANK(new Area(3250, 3419, 8, 6, 0)),
        VARROCK_WEST_BANK(new Area(3180, 3433, 6, 15, 0)),
        VER_SINHAZA_BANK(new Area(3646, 3204, 10, 13, 0)),
        WINTERTODT_BANK(new Area(1637, 3942, 4, 4, 0)),
        YANILLE_BANK(new Area(2609, 3088, 6, 10, 0));

        private final Area area;
        @Nullable
        private Predicate<WebContext> requirement;
    }
}
