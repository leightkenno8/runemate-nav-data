package com.runemate.web;

import com.runemate.web.transport.*;
import org.junit.*;

public class ValidationTest {

    @Test
    public void buildsFixedTransportsSuccessfully() {
        FixedTransports.load();
    }

    @Test
    public void buildsDynamicTransportsSuccessfully() {
        DynamicTransports.load();
    }

}
