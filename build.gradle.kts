plugins {
    java
    idea
    `java-library`
    `maven-publish`
    kotlin("jvm") version "1.6.10"
    id("io.freefair.lombok") version "6.3.0"
}

group = "com.runemate"
version = "1.3.12"

val externalRepositoryUrl: String by project
val externalRepositoryCredentialsName: String? by project
val externalRepositoryCredentialsValue: String? by project

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    testImplementation("junit:junit:4.13.2")
}

tasks {
    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
        }
    }
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }
    kotlin {
        jvmToolchain {
            (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of(11))
        }
    }
    test {
        useJUnit()
    }
}


publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryCredentialsValue != null) {
            credentials(HttpHeaderCredentials::class) {
                name = externalRepositoryCredentialsName ?: "Private-Token"
                value = externalRepositoryCredentialsValue
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
